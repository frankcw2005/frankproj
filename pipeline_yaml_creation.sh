script1=$(cat << 'EOF'
image: docker:latest

include:
- template: Code-Quality.gitlab-ci.yml
- template: Jobs/Dependency-Scanning.gitlab-ci.yml
- template: Jobs/SAST.gitlab-ci.yml
- template: Jobs/Secret-Detection.gitlab-ci.yml
EOF
)

echo "$script1" > security-config.yml
